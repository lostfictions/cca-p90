using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.Assertions;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class FlyCam : MonoBehaviour
{
    public bool domeEnabled;

    public float webSocketPointerAngleOffset;

    public GameObject domeRig;

    public CameraNode initialCameraNode;
    public bool freeFlyEnabled;

    public float turnSensitivity = 90;
    public float baseMoveSpeed = 10;
    public float slowMovementMultiplier = 0.25f;
    public float fastMovementMultiplier = 3;

    public float yMax = 80f;
    public float yMin = -80f;

    
    float rotationX;
    float rotationY;

    CameraNode currentCameraNode;
    Waiter transition;

    WebSocketPointer pointer;

    int layerMask;

    bool pendingTap;
    bool pendingCalibrate;

    bool isCalibrating;

    void OnEnable()
    {
        layerMask = 1 << LayerMask.NameToLayer("CameraNodeTarget");

        //Delay by a frame to avoid setup race condition
        Waiters.Wait(0, gameObject).Then(() => SetNode(initialCameraNode));
        
        pointer = GetComponent<WebSocketPointer>();
        Assert.IsNotNull(pointer);
        
        if(pointer.disable) {
            Cursor.lockState = CursorLockMode.Locked;
        }
        else {
            // Can only do raycasts/unity work on main thread, so just set a flag
            //FIXME: cleanup on disable
            pointer.OnTap += () => pendingTap = true;
            pointer.OnCalibrate += () => pendingCalibrate = true;
        }

        if(domeEnabled) {
            GetComponent<Camera>().enabled = false;
            domeRig.SetActive(true);
            transform.Find("Cursor").gameObject.SetActive(true);
        }
    }
    
    void Update()
    {
        if(!pointer.disable) {
            WebSocketUpdate();
        }
        else {
            ManualUpdate();
        }

        transform.localRotation = Quaternion.AngleAxis(rotationX, Vector3.up);
        transform.localRotation *= Quaternion.AngleAxis(rotationY, Vector3.left);
    }

    void WebSocketUpdate() {
        rotationX = pointer.Orientation.x * -1f + webSocketPointerAngleOffset;
        rotationY = Mathf.Clamp(pointer.Orientation.y, yMin, yMax);

        if(freeFlyEnabled) {
            float s = Input.GetAxis("Speed Multiplier");
            float movementSpeed = baseMoveSpeed * (s < 0 ? Mathf.Lerp(1f, slowMovementMultiplier, -s) : Mathf.Lerp(1f, fastMovementMultiplier, s));

            Vector3 pos = transform.localPosition;
            pos += transform.forward * movementSpeed * pointer.MovementDelta / 200f * Time.deltaTime;
            transform.localPosition = pos;
        }

        if(pendingTap) {
            pendingTap = false;
            DoTap();
        }

        if(pendingCalibrate) {
            pendingCalibrate = false;

            isCalibrating = !isCalibrating;
            Debug.Log("Calibrating: " + isCalibrating);
        }
    }

    void ManualUpdate() {
        rotationX += Input.GetAxis("Horizontal Look") * turnSensitivity * Time.deltaTime;
        rotationY += Input.GetAxis("Vertical Look") * turnSensitivity * Time.deltaTime;
        rotationY = Mathf.Clamp(rotationY, yMin, yMax);

        if(Input.GetButtonDown("Back")) {
            freeFlyEnabled = !freeFlyEnabled;
        }

        if(freeFlyEnabled) {
            float s = Input.GetAxis("Speed Multiplier");
            float movementSpeed = baseMoveSpeed * (s < 0 ? Mathf.Lerp(1f, slowMovementMultiplier, -s) : Mathf.Lerp(1f, fastMovementMultiplier, s));

            Vector3 pos = transform.localPosition;
            pos += transform.forward * movementSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
            pos += transform.right * movementSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;
            pos += transform.up * movementSpeed * Input.GetAxis("UpDown") * Time.deltaTime;
            transform.localPosition = pos;
        }

        if(Input.GetKeyDown(KeyCode.BackQuote)) {
            Cursor.lockState = (Cursor.lockState == CursorLockMode.Locked) ? CursorLockMode.None : CursorLockMode.Locked;
        }

        if(Input.GetButtonDown("Fire1")) {
            DoTap();
        }
    }

    void DoTap() {
        RaycastHit hit;
        if(Physics.Raycast(transform.position, transform.forward, out hit, 5f, layerMask, QueryTriggerInteraction.Collide)) {
            var nt = hit.transform.GetComponent<CameraNodeTarget>();
            Assert.IsNotNull(nt);
            if(transition == null) {
                SetNode(nt.targetNode);
            }
        }
    }

    public void SetNode(CameraNode node, bool instantaneous = false)
    {
        if(node == currentCameraNode) {
            return;
        }

        if(currentCameraNode != null) {
            currentCameraNode.OnNodeExit();
        }
        if(node != null) {
            node.OnNodeEnter();
            if(instantaneous) {
                transform.position = node.transform.position;
            }
            else {
                var start = transform.position;
                var end = node.transform.position;
                transition = Waiters.Interpolate(1.3f, t => transform.position = Vector3.Lerp(start, end, Easing.EaseInOut(t, EaseType.Quad)), gameObject);
            }
        }
        currentCameraNode = node;
    }
}
