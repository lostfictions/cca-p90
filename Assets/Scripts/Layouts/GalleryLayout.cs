using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GalleryLayout : IPhotoLayout
{
    public Waiter OnEnter(bool instant = false)
    {
        var meta = PhotoLayouts.rawMetadata;

        var transitions = new List<TransitionInfo>();

        var images = meta
                    .Select(d => new {
                        d.importance,
                        d.subject,
                        d.index,
                        transform = PhotoLayouts.GetPhotoTransform(d.id)
                    })
                    .OrderBy(d => d.subject)
                    .ThenBy(d => d.index)
                    .ToArray();

        for(int j = 0; j < images.Length; j++) {
            float importanceFactor = Mathy.Map(images[j].importance, 3f, 1f, 1f, 2f);


            var bounding = GameObject.Find("BoundingSphere").transform;

            var targetPos = bounding.localPosition + new Vector3(0, bounding.localScale.y, 0) + new Vector3(Random.Range(-5f, 5f) * 30f, Random.value * 10f, Random.Range(-5f, 5f) * 30f);
            var targetRot = Random.value > 0.5f ? Quaternion.AngleAxis(90f, Vector3.up) : Quaternion.identity;
            var targetScale = Vector3.one * importanceFactor;

            var t = images[j].transform;
            if(instant) {
                t.localPosition = targetPos;
                t.localRotation = targetRot;
                t.localScale = targetScale;
            }
            else {
                transitions.Add(new TransitionInfo {
                    transform = t,
                    initPos = t.localPosition,
                    initRot = t.localRotation,
                    initScale = t.localScale,
                    targetPos = targetPos,
                    targetRot = targetRot,
                    targetScale = targetScale
                });
            }
        }


        if(!instant) {
            return Waiters
                .Interpolate(4f, f => {
                    float eased = Easing.EaseInOut(f, EaseType.Cubic);
                    for(int i = 0; i < transitions.Count; i++) {
                        var t = transitions[i];
                        var trans = t.transform;
                        trans.localPosition = Vector3.Lerp(t.initPos, t.targetPos, eased);
                        trans.localRotation = Quaternion.Lerp(t.initRot, t.targetRot, eased);
                        trans.localScale = Vector3.Lerp(t.initScale, t.targetScale, eased);
                    }
                });
        }

        return null;
    }

    public void OnTick()
    {

    }

    public Waiter OnExit(bool instant = false)
    {
        if(instant) {
            return null;
        }
        return Waiters.Wait(0);
    }
}
