using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TimelineLayout : IPhotoLayout
{
    Transform linearSegmentsParent;

    public Waiter OnEnter(bool instant = false)
    {
        var meta = PhotoLayouts.rawMetadata;

        if(linearSegmentsParent == null) {
            CreateTimelineSegments(meta);
        }
        linearSegmentsParent.gameObject.SetActive(true);

        int min = meta.Min(d => d.year);
        int max = meta.Max(d => d.year);

        var years = Enumerable
            .Range(min, max - min + 1)
            .Reverse()
            .ToArray();

        var transitions = new List<TransitionInfo>();
        for(int k = 0; k < years.Length; k++) {
            int year = years[k];

            var categories = meta
                .Where(d => d.year == year)
                .GroupBy(d => d.category)
                .ToArray();

            int categoryCount = categories.Length;

            for(int i = 0; i < categoryCount; i++) {
                float angle = 2f * Mathf.PI * i / categoryCount;
                angle += Mathf.PI * k / 2.3f; //Rotate initial angle by an offset each year

                Vector3 direction = new Vector3(Mathf.Sin(angle), Mathf.Cos(angle), 0);

                var images = categories[i]
                    .Select(d => new {
                        d.importance,
                        d.subject,
                        d.index,
                        transform = PhotoLayouts.GetPhotoTransform(d.id)
                    })
                    .OrderBy(d => d.subject)
                    .ThenBy(d => d.index)
                    .ToArray();

                Vector3 positionOffset = Vector3.zero;
                for(int j = 0; j < images.Length; j++) {
                    float importanceFactor = Mathy.Map(images[j].importance, 3f, 1f, 1f, 2f);
                    positionOffset += direction * (5f + importanceFactor) + (Vector3.forward * j / images.Length * 4.5f);

                    var targetPos = (Vector3.forward * 5f * k) + positionOffset;
                    var targetRot = Quaternion.identity;
                    var targetScale = Vector3.one * importanceFactor;


                    var t = images[j].transform;
                    if(instant) {
                        t.localPosition = targetPos;
                        t.localRotation = targetRot;
                        t.localScale = targetScale;
                    }
                    else {
                        transitions.Add(new TransitionInfo {
                            transform = t,
                            initPos = t.localPosition,
                            initRot = t.localRotation,
                            initScale = t.localScale,
                            targetPos = targetPos,
                            targetRot = targetRot,
                            targetScale = targetScale
                        });
                    }
                }
            }
        }

        if(!instant) {
            return Waiters
                .Interpolate(4f, f => {
                    float eased = Easing.EaseInOut(f, EaseType.Cubic);
                    for(int i = 0; i < transitions.Count; i++) {
                        var t = transitions[i];
                        var trans = t.transform;
                        trans.localPosition = Vector3.Lerp(t.initPos, t.targetPos, eased);
                        trans.localRotation = Quaternion.Lerp(t.initRot, t.targetRot, eased);
                        trans.localScale = Vector3.Lerp(t.initScale, t.targetScale, eased);
                    }
                });
        }
        return null;
    }

    void CreateTimelineSegments(PhotoMetadata[] meta)
    {
        var pl = PhotoLayouts.Instance;

        linearSegmentsParent = new GameObject("LinearSegments").transform;
        linearSegmentsParent.parent = pl.transform;
        linearSegmentsParent.localPosition = Vector3.zero;

        int min = meta.Min(d => d.year);
        int max = meta.Max(d => d.year);

        var years = Enumerable
            .Range(min, max - min + 1)
            .Reverse()
            .ToArray();

        for(int k = 0; k < years.Length; k++) {
            int year = years[k];

            var lineSegment = Object.Instantiate(pl.linePrefab, linearSegmentsParent);
            lineSegment.localPosition = Vector3.forward * 5f * k;
            lineSegment.name = year.ToString();

            var canvas = Object.Instantiate(pl.textCanvasPrefab, lineSegment);
            canvas.localPosition = Vector3.zero;
            var t = canvas.GetComponentInChildren<Text>();
            t.text = year.ToString();
        }
    }

    public void OnTick()
    {
        
    }

    public Waiter OnExit(bool instant = false)
    {
        linearSegmentsParent.gameObject.SetActive(false);
        if(instant) {
            return null;
        }
        return Waiters.Wait(0);
    }
}
