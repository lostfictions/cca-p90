using System.ComponentModel.Design.Serialization;
using JetBrains.Annotations;
using UnityEngine;
using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using UnityEngine.Assertions;

public class PhotoLayouts : MonoBehaviour
{
    public TextAsset metadataJson;

    [Header("Prefabs")]
    public Transform textCanvasPrefab;
    public Transform linePrefab;
    public SpriteRenderer spritePrefab;

    public static PhotoMetadata[] rawMetadata;
    public static Dictionary<string, Transform> imageTransforms;

    Transform photosParent;

    PhotoLayoutMachine stateMachine;

    PhotoLayoutMachine.Layouts? pendingLayout;

    static PhotoLayouts instance;
    public static PhotoLayouts Instance
    {
        get
        {
            if(instance == null) {
                instance = FindObjectOfType<PhotoLayouts>();
            }
            if(instance == null) {
                throw new Exception("No instantiated " + typeof(PhotoLayouts).FullName);
            }
            return instance;
        }
    }

    public static Transform GetPhotoTransform(string id)
    {
        Transform t;
        if(!imageTransforms.TryGetValue(id, out t)) {
            var sprite = Resources.Load<Sprite>("Photos/thumb_" + id);
            Assert.IsNotNull(sprite, "No sprite at 'Photos/thumb_" + id);
            var s = Instantiate(Instance.spritePrefab);
            s.sprite = sprite;
            t = s.transform;
            t.parent = Instance.photosParent;
            imageTransforms.Add(id, t);
        }
        return t;
    }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        rawMetadata = JsonUtility.FromJson<PhotoMetadataSet>(metadataJson.text).data;

        imageTransforms = new Dictionary<string, Transform>(rawMetadata.Length);

        var pointer = FindObjectOfType<WebSocketPointer>();
        if(pointer != null && !pointer.disable) {
            pointer.OnLayout += layout => pendingLayout = layout;
        }

        photosParent = new GameObject("Photos").transform;
        photosParent.parent = transform;
        photosParent.localPosition = Vector3.zero;

        stateMachine = new PhotoLayoutMachine(PhotoLayoutMachine.Layouts.Timeline);
    }


    
    void Update()
    {
        stateMachine.Tick();

        if(pendingLayout.HasValue) {
            stateMachine.SetLayout(pendingLayout.Value);
            pendingLayout = null;
        }

        if(Input.GetKeyDown(KeyCode.Alpha1)) {
            stateMachine.SetLayout(PhotoLayoutMachine.Layouts.Timeline);
        }
        else if(Input.GetKeyDown(KeyCode.Alpha2)) {
            stateMachine.SetLayout(PhotoLayoutMachine.Layouts.Radial);
        }
        else if(Input.GetKeyDown(KeyCode.Alpha3)) {
            stateMachine.SetLayout(PhotoLayoutMachine.Layouts.Gallery);
        }
    }
}


internal struct TransitionInfo
{
    public Transform transform;
    public Vector3 initPos;
    public Quaternion initRot;
    public Vector3 initScale;
    public Vector3 targetPos;
    public Quaternion targetRot;
    public Vector3 targetScale;
}

/// For deserializing from JSON only -- can't be an unnamed array.
[Serializable]
public class PhotoMetadataSet
{
    public PhotoMetadata[] data;
}

[Serializable]
public class PhotoMetadata
{
    public string id;
    public string filename;

    public int year;
    public int importance;
    public string category;

    public string subject;
    public int index;
}

public class PhotoLayoutMachine
{
    /// yes, this is not good oop. shhh. none of this is. this needs to get done.
    public enum Layouts { Timeline, Radial, Gallery }

    Layouts currentLayout;
    public IPhotoLayout CurrentLayout { get; private set; }

    Dictionary<Layouts, IPhotoLayout> layouts = new Dictionary<Layouts, IPhotoLayout>();
    Waiter transition;

    public PhotoLayoutMachine(Layouts initialLayout)
    {
        layouts[Layouts.Timeline] = new TimelineLayout();
        layouts[Layouts.Radial] = new RadialLayout();
        layouts[Layouts.Gallery] = new GalleryLayout();

        currentLayout = initialLayout;
        CurrentLayout = layouts[currentLayout];
        CurrentLayout.OnEnter(true);
    }

    public void Tick()
    {
        if(transition == null) {
            CurrentLayout.OnTick();
        }
    }

    public void SetLayout(Layouts layout, bool instant = false)
    {
        if(currentLayout == layout) {
            return;
        }
        IPhotoLayout nextLayout = layouts[layout];

        if(transition != null) {
            Object.Destroy(transition);
        }
        
        if(instant) {
            CurrentLayout.OnExit(true);
            currentLayout = layout;
            CurrentLayout = nextLayout;
            CurrentLayout.OnEnter(true);
        }
        else {
            transition = CurrentLayout.OnExit();
            transition.Then(() => {
                currentLayout = layout;
                CurrentLayout = nextLayout;
                transition = CurrentLayout.OnEnter();
            });
        }
    }
}

public interface IPhotoLayout {
    Waiter OnEnter(bool instant = false);
    void OnTick();
    Waiter OnExit(bool instant = false);
}

