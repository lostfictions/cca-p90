/*global GyroNorm:false Hammer:false*/
'use strict'

const DEBUG = false

const MESSAGE_TYPES = {
  ORIENTATION: 0,
  MOVEMENT: 1,
  TAP: 2,
  LAYOUT: 3,
  CALIBRATE: 4
}

// let messages
let elStatus
let websocket

function trySend(msg) {
  if(websocket != null &&
    websocket.readyState &&
    websocket.readyState !== WebSocket.CONNECTING &&
    websocket.readyState !== WebSocket.CLOSING &&
    websocket.readyState !== WebSocket.CLOSED) {
    websocket.send(JSON.stringify(msg))
  }
}

let delta = 0

function init() {
  elStatus = document.getElementById("status")
  // messages = document.getElementById("messages")

  const gn = new GyroNorm()

  gn.init().then(function() {
    gn.start(function(data) {
      trySend({ messageType: MESSAGE_TYPES.ORIENTATION, yaw: data.do.alpha, pitch: data.do.beta })
    })
  }).catch(function(e) {
    // DeviceOrientation or DeviceMotion not supported by browser or device
  })

  const touchpad = document.getElementById('touchpad')

  const titleArea = document.getElementById('title-area')
  titleArea.onclick = () => document.body.webkitRequestFullscreen()

  // const h = new Hammer(document.body)
  const h = new Hammer(touchpad)

  h.on("tap", function(ev) {
    // touchpad.textContent = ev.type + " gesture detected. ";
    touchpad.style.backgroundColor = null
    trySend({ messageType: MESSAGE_TYPES.TAP })
  })

  h.on("hammer.input", function(ev) {
    if(ev.isFinal) {
      touchpad.style.backgroundColor = null
      trySend({ messageType: MESSAGE_TYPES.MOVEMENT, delta: 0 })
    }
    else {
      delta = clamp(-ev.deltaY, -200, 200)
      trySend({ messageType: MESSAGE_TYPES.MOVEMENT, delta: delta })
      touchpad.style.backgroundColor = "hsl(" + map(delta, -200, 200, 0, 120) + ", " + Math.round(Math.abs(delta) / 2) + "%, 50%)"
    }
  })

  const buttonLayoutSphere = document.getElementById('button-layout-sphere')
  const buttonLayoutGallery = document.getElementById('button-layout-gallery')
  const buttonLayoutTimeline = document.getElementById('button-layout-timeline')
  const buttonCalibrate = document.getElementById('button-calibrate')

  buttonLayoutSphere.onclick = () => trySend({ messageType: MESSAGE_TYPES.LAYOUT, layout: "SPHERE" })
  buttonLayoutGallery.onclick = () => trySend({ messageType: MESSAGE_TYPES.LAYOUT, layout: "GALLERY" })
  buttonLayoutTimeline.onclick = () => trySend({ messageType: MESSAGE_TYPES.LAYOUT, layout: "TIMELINE" })

  buttonCalibrate.onclick = () => trySend({ messageType: MESSAGE_TYPES.CALIBRATE })

  setInterval(function() {
    if(websocket == null) {
      doWebSocket()
    }
  }, 2000)
}

// function lerp(from, to, t) {
//   return from * (1 - t) + to * t
// }

function clamp(val, min, max) {
  if(val < min) return min
  if(val > max) return max
  return val
}

function map(val, fromMin, fromMax, toMin, toMax) {
  //normalize val to 0..1 range
  val = (val - fromMin) / (fromMax - fromMin);
  //then map to other domain
  return val * (toMax - toMin) + toMin;
}


function doWebSocket() {
  const url = 'ws://' + document.location.host + '/Pointer'

  websocket = new WebSocket(url)

  websocket.onopen = function() {
    elStatus.innerHTML = '<span class="text-success">CONNECTED</span>'
  }

  websocket.onmessage = function(event) {
    if(DEBUG) {
      writeToScreen('<span class="text-info">RESPONSE: ' + event.data + '</span>')
    }
  }

  websocket.onerror = function(event) {
    if(DEBUG) {  
      writeToScreen('<span class="text-warning">ERROR: ' + event.data + '</span>')
    }
  }

  websocket.onclose = function onClose(event) {
    elStatus.innerHTML = '<span class="text-warning">DISCONNECTED</span>'
    setTimeout(function() { websocket = null }, 100)
  }
}

function writeToScreen(message) {
  // var pre = document.createElement("p")
  // pre.style.wordWrap = "break-word"
  // pre.innerHTML = message
  // messages.appendChild(pre)
}

window.addEventListener("load", init, false)
