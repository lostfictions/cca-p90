using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

using WebSocketSharp;
using WebSocketSharp.Server;
using System.Net;
using System.Net.Sockets;
using HttpStatusCode = WebSocketSharp.Net.HttpStatusCode;

public class WebSocketPointer : MonoBehaviour
{
    public bool disable;

    public int port = 4649;

    HttpServer server;

    public Vector2 Orientation { get; internal set; }
    public float MovementDelta { get; internal set; }

    public event Action OnTap;
    internal void InvokeOnTap() {
        if(OnTap != null) {
            OnTap();
        }
    }

    public event Action<PhotoLayoutMachine.Layouts> OnLayout;
    internal void InvokeOnLayout(PhotoLayoutMachine.Layouts layout)
    {
        if(OnLayout != null) {
            OnLayout(layout);
        }
    }

    public event Action OnCalibrate;
    internal void InvokeOnCalibrate()
    {
        if(OnCalibrate != null) {
            OnCalibrate();
        }
    }

    public bool IsListening { get { return server.IsListening; } }
    public string Host { get; private set; }

    void Awake()
    {
        if(disable) {
            return;
        }

        server = new HttpServer(port) {
            RootPath = Path.Combine(Application.streamingAssetsPath, "Public").Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar)
        };

        server.OnGet += (sender, e) => {
            var req = e.Request;
            var res = e.Response;

            var path = req.RawUrl;
            if(path == "/") {
                path += "index.html";
            }

            var content = server.GetFile(path);
            if(content == null) {
                res.StatusCode = (int)HttpStatusCode.NotFound;
                return;
            }

            if(path.EndsWith(".html")) {
                res.ContentType = "text/html";
                res.ContentEncoding = Encoding.UTF8;
            }
            else if(path.EndsWith(".js")) {
                res.ContentType = "application/javascript";
                res.ContentEncoding = Encoding.UTF8;
            }

            res.WriteContent(content);
        };

        server.AddWebSocketService("/Pointer", () => new Pointer(this));

        server.Start();

        Host = IPUtil.GetLocalIPAddress() + ":" + port;
        Debug.Log("Listening at: " + Host);
    }
    void OnDestroy()
    {
        if(server != null) {
            server.Stop();
            server = null;
        }
    }
}

internal class Pointer : WebSocketBehavior
{
    enum MessageType {
        Orientation = 0,
        Movement,
        Tap,
        Layout,
        Calibrate
    }
    

    struct PointerData
    {
        public int messageType;

        //Orientation       
        public float yaw;
        public float pitch;

        //Movement
        public float delta;

        //Layout
        public string layout;
    }

    readonly WebSocketPointer parent;

    public Pointer(WebSocketPointer parent)
    {
        this.parent = parent;
    }
    protected override void OnMessage(MessageEventArgs e)
    {
        var data = JsonUtility.FromJson<PointerData>(e.Data);

        switch(data.messageType) {
            case (int)MessageType.Orientation:
                parent.Orientation = new Vector2(data.yaw, data.pitch);
            break;
            case (int)MessageType.Movement:
                parent.MovementDelta = data.delta;
            break;
            case (int)MessageType.Tap:
                parent.InvokeOnTap();
            break;
            case (int)MessageType.Layout:
                switch(data.layout) {
                    case "SPHERE": parent.InvokeOnLayout(PhotoLayoutMachine.Layouts.Radial); break;
                    case "GALLERY": parent.InvokeOnLayout(PhotoLayoutMachine.Layouts.Gallery); break;
                    case "TIMELINE": parent.InvokeOnLayout(PhotoLayoutMachine.Layouts.Timeline); break;
                    default: throw new Exception("Unknown layout type: " + data.layout);
                }
            break;
            case (int)MessageType.Calibrate:
                parent.InvokeOnCalibrate();
            break;
        }
    }
}


//From: https://stackoverflow.com/questions/6803073/get-local-ip-address
internal static class IPUtil
{
    public static string GetLocalIPAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach(var ip in host.AddressList) {
            if(ip.AddressFamily == AddressFamily.InterNetwork) {
                return ip.ToString();
            }
        }
        throw new Exception("Local IP Address Not Found!");
    }
}
