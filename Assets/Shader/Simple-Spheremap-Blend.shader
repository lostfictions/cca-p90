Shader "Custom/Simple Spheremap Blend" {
	Properties {
		_CubeATex("Cubemap", CUBE) = "" {}
		_CubeBTex("Cubemap", CUBE) = "" {}
		_value ("Value", Range (0, 1)) = 0.5
	}
		SubShader {
		Tags { "RenderType" = "Opaque" }

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"
			samplerCUBE _CubeATex;
			samplerCUBE _CubeBTex;
      float _value;
			struct appdata_t {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};
			struct v2f {
				float4 vertex : POSITION;
				float3 texcoord : TEXCOORD0;
			};
			v2f vert(appdata_t v) {
				v2f OUT;
				OUT.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				OUT.texcoord = v.normal;
				return OUT;
			}
			half4 frag(v2f IN) : COLOR{
				return lerp(texCUBE(_CubeATex, IN.texcoord), texCUBE(_CubeBTex, IN.texcoord), _value);
			}
				ENDCG
		}
	}
	FallBack "Diffuse"
}

