using UnityEngine;
using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class CameraNode : MonoBehaviour
{
    [Header("Prefabs")]
    public CameraNodeTarget targetPrefab;

    [Header("Scene References")]
    public CameraNode[] adjacentNodes;

    CameraNodeTarget[] targets;

    void Awake()
    {
        targets = new CameraNodeTarget[adjacentNodes.Length];

        for(int i = 0; i < adjacentNodes.Length; i++) {
            var adjacentNode = adjacentNodes[i];

            var target = Instantiate(targetPrefab);
            target.targetNode = adjacentNode;
            targets[i] = target;

            Vector3 direction = (adjacentNode.transform.position - transform.position).normalized;

            var t = target.transform;
            t.forward = direction;
            t.SetParent(transform, true);
            t.localPosition = direction * 3.5f;

            target.gameObject.SetActive(false);
        }
    }

    public void OnNodeEnter()
    {
        foreach(var target in targets) {
            target.gameObject.SetActive(true);
        }
    }

    public void OnNodeExit()
    {
        foreach(var target in targets) {
            target.gameObject.SetActive(false);
        }
    }
}
