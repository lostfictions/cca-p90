const csv = require('csv')
const fs = require('fs')

const fieldsToDelete = [
    "type", "file_kind", "file_url", "foldername", "folder_id", "create_date", "change_date", "expiry_date", "labels",
    "keywords", "description", "iptcsubjectcode", "creator", "title", "authorstitle", "descwriter", "iptcaddress", "category",
    "categorysub", "urgency", "iptccity", "iptccountry", "iptclocation", "iptczip", "iptcemail", "iptcwebsite", "iptcphone",
    "iptcintelgenre", "iptcinstructions", "iptcsource", "iptcusageterms", "copystatus", "iptcjobidentifier", "copyurl",
    "iptcheadline", "iptcdatecreated", "iptcimagecity", "iptcimagestate", "iptcimagecountry", "iptcimagecountrycode",
    "iptcscene", "iptcstate", "iptccredit", "copynotice", "pdf_author", "pdf_rights", "pdf_authorsposition",
    "pdf_captionwriter", "pdf_webstatement", "pdf_rightsmarked"
]

const fieldsToRename = {
    "medium:866acecd-bba9-48fb-961b8cd7efc77819": "medium",
    "auteur_créateur:f4c5a144-7a5c-413c-95a2ca2a1b32bc87": "creator",
    "temps:61e5b6da-c565-41c9-8c245afc1e5d949d": "time",
    "lieu:b1e0218c-576a-44bf-b45da1fc0d42436d": "place",
    "catégorie:a142ba38-c267-43b5-9b9be5d22470bfd0": "category",
    "sujet_projet_thématique:2f51db68-74dc-4f9f-bf186b8c85e7b993": "subject",
    "personnes_identifiables:71a9632e-1219-4432-ae3db0beffa846cb": "people",
    "importance:d71b618c-bc3d-453a-847c64976ef8d01d": "importance"
}


csv.parse(fs.readFileSync('metadata.csv'), { columns: true }, (err, data) => {
    const newData = data
        .map(d => {
            fieldsToDelete.forEach(f => delete d[f])

            const nextD = {}
            for(const i in d) {
                if(d.hasOwnProperty(i)) {
                    if(i in fieldsToRename) {
                        nextD[fieldsToRename[i]] = d[i]
                    }
                    else {
                        nextD[i] = d[i]
                    }
                }
            }

            return nextD
        })
        .filter(d => d.id)
        .map(d => {
            const nextD = Object.assign({}, d)
            nextD.importance = Number.parseInt(nextD.importance.split(' ')[0], 10)
            if(Number.isNaN(nextD.importance)) {
                // console.warn(`'${nextD.filename}' has invalid importance value!`)
                nextD.importance = 2
            }

            const [year, category, subject, series ] = nextD.filename.substring(0, nextD.filename.indexOf('.')).split('_')
            
            if(category !== nextD.category) {
                if(category === "Seagram" && nextD.category === "Seagrams") {
                    nextD.category = "Seagram"
                }
                else if(nextD.category == '' && category != null) {
                    nextD.category = category
                }
                else {
                    console.warn(`${nextD.filename} category ${category} != ${nextD.category}!`)
                }
            }
            nextD.year = Number.parseInt(year, 10)
            if(Number.isNaN(nextD.year)) {
                console.warn(`'${nextD.filename}' has invalid year!`)
            }
            nextD.subject = subject
            
            const index = Number.parseInt(series, 10)
            if(Number.isNaN(index)) {
                nextD.index = 0
            }
            else {
                //Correct 1-indexing
                nextD.index = index - 1
            }

            //temp
            delete nextD["medium"]
            delete nextD["creator"]
            delete nextD["time"]
            delete nextD["place"]
            delete nextD["people"]

            return nextD
        })


    /////////
    //Quick histogram.
    /////////
    // const years = {}
    // const categories = {}
    // for(const d of newData) {
    //     if(d.year in years) {
    //         years[d.year]++
    //     }
    //     else {
    //         years[d.year] = 1
    //     }

    //     if(d.category in categories) {
    //         categories[d.category]++
    //     }
    //     else {
    //         categories[d.category] = 1
    //     }
    // }
    // console.dir(years)
    // console.dir(categories)
    /////////

    fs.writeFileSync('metadata.json', JSON.stringify({ data: newData }, undefined, 2))
})
